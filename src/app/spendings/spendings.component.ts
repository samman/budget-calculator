import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-spendings',
  templateUrl: './spendings.component.html',
  styleUrls: ['./spendings.component.css']
})
export class SpendingsComponent implements OnInit {
  @Input() amounts: Array<object> = [];
  @Output() remove: EventEmitter<object> = new EventEmitter<object>()

  constructor() { }

  ngOnInit(): void {
  }

  onClick(index: number){
    this.remove.emit({index})
  }

}
