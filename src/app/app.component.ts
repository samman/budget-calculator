import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  budget: number = 0;
  
  amounts: Array<any> = [];

  newAmount: number;
  newDescription: string="";

  title = 'budget-calculator';

  onClick(){
    if(!this.newAmount){
      return;
    }
    this.amounts.push({amount: this.newAmount, description: this.newDescription});
    this.calculateBudget(this.newAmount);
  }

  calculateBudget(num: number){
    this.budget = this.budget + num;
  }

  handleRemove(index: number){
    const amount = this.amounts[index].amount - (this.amounts[index].amount*2);
    this.calculateBudget(amount);
    this.amounts.splice(index,1);
  }
}
